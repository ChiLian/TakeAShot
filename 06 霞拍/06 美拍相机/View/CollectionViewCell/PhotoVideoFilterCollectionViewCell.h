//
//  PhotoVideoFilterCollectionViewCell.h
//  06 美拍相机
//
//  Created by dllo on 16/3/18.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoVideoFilterCollectionViewCell : UICollectionViewCell

// 显示特效的图片
@property (strong, nonatomic) IBOutlet UIImageView *showFilterImageView;

// 显示特效的名称
@property (strong, nonatomic) IBOutlet UILabel *showFilterName;


@end
