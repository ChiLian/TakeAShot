//
//  NHPlayerView.h
//  17  视频播放器
//
//  Created by dllo on 16/2/26.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NHPlayerView : UIView

@property(nonatomic, retain)NSString *playerURLStr;

// 初始化方法
- (instancetype)initWithFrame:(CGRect)frame;

// 开始播放
- (void)playerPlay;
// 暂停播放
- (void)playerPause;

@end
