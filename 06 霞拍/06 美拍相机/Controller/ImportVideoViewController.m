//
//  ImportVideoViewController.m
//  06 美拍相机
//
//  Created by dllo on 16/3/18.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "ImportVideoViewController.h"
#import "PhotoCollectionViewCell.h"

#import "PhotoVideoViewController.h"

#import <AssetsLibrary/AssetsLibrary.h>

#import "GPUImage.h"

@interface ImportVideoViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>

// 显示视频的缩略图
@property (strong, nonatomic) IBOutlet UICollectionView *showVideoCollectionView;
// 保存视频的缩略图
@property(nonatomic, strong)NSMutableArray *videoThumbnailArr;
// 保存视频的URL链接
@property(nonatomic, strong)NSMutableArray *videoURLArr;

// 获得相册
@property(nonatomic, strong)ALAssetsLibrary *library;

// 占位视图
@property (strong, nonatomic) IBOutlet UILabel *placeholder;
@end

@implementation ImportVideoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // 创建视图
    [self createView];
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    // 获得视频缩略图
    [self getVideo];
}


#pragma mark 创建视图
- (void)createView{
    
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([PhotoCollectionViewCell class]) bundle:nil];
    
    [self.showVideoCollectionView registerNib:nib forCellWithReuseIdentifier:NSStringFromClass([PhotoCollectionViewCell class])];
    
}
#pragma mark UICollectionView的协议方法
// 个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    
    return self.videoThumbnailArr.count;
}
// 创建Cell
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    PhotoCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([PhotoCollectionViewCell class]) forIndexPath:indexPath];
    
    cell.imageView.image = self.videoThumbnailArr[indexPath.row];
    
    return cell;
}
// 点击方法
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    
    NSLog(@"%@",self.videoURLArr[indexPath.row]);
    
    PhotoVideoViewController *photoVideoVC = [[PhotoVideoViewController alloc] initWithNibName:NSStringFromClass([PhotoVideoViewController class]) bundle:nil];
    photoVideoVC.videoURL = self.videoURLArr[indexPath.row];
    [self presentViewController:photoVideoVC animated:YES completion:^{
        
        
    }];
}

#pragma mark 自定义方法
/**
 *  获得视频的缩略图
 */
- (void)getVideo{
    
    self.library = [[ALAssetsLibrary alloc] init];
    
    self.videoThumbnailArr = [NSMutableArray array];
    
    self.videoURLArr = [NSMutableArray array];
    
    [self.library enumerateGroupsWithTypes:ALAssetsGroupAll usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
        
        // 设置相册的筛选条件,现在选择的是只包含图片
        [group setAssetsFilter:[ALAssetsFilter allVideos]];
        
        [group enumerateAssetsUsingBlock:^(ALAsset *result, NSUInteger index, BOOL *stop) {
            
            CGImageRef ref = [result thumbnail];
            
            NSURL *url = [[result defaultRepresentation] url];
            
            UIImage *img = [[UIImage alloc] initWithCGImage:ref];
            if(img != nil){
                [self.videoThumbnailArr addObject:img];
                [self.videoURLArr addObject:url];
            }
        }];
        
        if(self.videoURLArr.count == 0){
            [self showPlaceholder];
        }
        else{
            [self hidePlaceholder];
            [self.showVideoCollectionView reloadData];
        }
    } failureBlock:^(NSError *error) {
        
        
    }];
}
#pragma mark  当没有视频的时候显示
/**
 *  显示占位视图
 */
- (void)showPlaceholder{
    self.placeholder.alpha = 1.0;
}

/**
 *  隐藏占位视图
 */
- (void)hidePlaceholder{
    self.placeholder.alpha = 0.0;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark UI方法 退出按钮

// 点击了退出按钮
- (IBAction)clickQuit:(id)sender {
    
    
    [self dismissViewControllerAnimated:YES completion:^{
        
        
    }];

}

@end
