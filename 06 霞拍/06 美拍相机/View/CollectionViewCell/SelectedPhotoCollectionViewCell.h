//
//  SelectedPhotoCollectionViewCell.h
//  06 美拍相机
//
//  Created by dllo on 16/3/18.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SelectedPhotoCollectionViewCell : UICollectionViewCell

// 删除按钮
@property (strong, nonatomic) IBOutlet UIButton *deleteButton;

// 显示被选中的图片
@property (strong, nonatomic) IBOutlet UIImageView *showSelectedImageView;
@end
