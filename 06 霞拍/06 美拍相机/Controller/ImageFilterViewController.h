//
//  ImageFilterViewController.h
//  06 美拍相机
//
//  Created by dllo on 16/3/21.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageFilterViewController : UIViewController

// 图片
@property(nonatomic, strong)UIImage *image;

@end