//
//  ImageFilterViewController.m
//  06 美拍相机
//
//  Created by dllo on 16/3/21.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import "ImageFilterViewController.h"
#import "GPUImage.h"
#import "PhotoVideoFilterCollectionViewCell.h"

@interface ImageFilterViewController ()<UICollectionViewDataSource,UICollectionViewDelegate>
// 显示滤镜效果的CollectionView
@property (strong, nonatomic) IBOutlet UICollectionView *filterCollectionView;

// 显示图片的视图
@property (strong, nonatomic) IBOutlet UIImageView *showImageView;


// 保存Filter的数组
@property(nonatomic, strong)NSMutableArray *filterArr;

// 保存Filter的名称
@property(nonatomic, strong)NSMutableArray *filterNameArr;

// 记录当前是否在编辑状态
@property(nonatomic, assign)BOOL isEdit;

// 保存添加滤镜后的图片
@property(nonatomic, strong)UIImage *filterImage;
@end

@implementation ImageFilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    // 初始化编辑状态
    self.isEdit = YES;
    
    // 创建数据
    [self createData];
    
    // 创建滤镜
    [self createFilter];
    
    // 创建视图
    [self createView];
}
#pragma mark 创建数据
- (void)createData{
    
    self.filterNameArr = [NSMutableArray arrayWithObjects:@"原图",@"流年",@"聚焦",@"油画",@"荧光",@"素描",@"夜色",@"模糊", nil];
}
#pragma mark 创建滤镜
- (void)createFilter{
    self.filterArr = [NSMutableArray array];
    
    // 0 褐色(怀旧)
    GPUImageSepiaFilter *imageSepiaFilter = [[GPUImageSepiaFilter alloc] init];
    [self.filterArr addObject:imageSepiaFilter];
    
    // 1 晕影，形成黑色圆形边缘，突出中间图像的效果
    GPUImageVignetteFilter *imageVignetteFilter = [[GPUImageVignetteFilter alloc] init];
    [self.filterArr addObject:imageVignetteFilter];
    
    // 2 卡通效果（黑色粗线描边）
    GPUImageToonFilter *imageToonFilter = [[GPUImageToonFilter alloc] init];
    [self.filterArr addObject:imageToonFilter];
    
    // 3 阈值边缘检测（效果与上差别不大）
    GPUImageThresholdEdgeDetectionFilter *imageThresholdEdgeDetectionFilter = [[GPUImageThresholdEdgeDetectionFilter alloc] init];
    [self.filterArr addObject:imageThresholdEdgeDetectionFilter];
    
    // 4 素描 4
    GPUImageSketchFilter *imageSketchFilter = [[GPUImageSketchFilter alloc] init];
    [self.filterArr addObject:imageSketchFilter];
    
    // 5 SoftElegance lookup
//    GPUImageSoftEleganceFilter *imageSoftEleganceFilter = [[GPUImageSoftEleganceFilter alloc] init];
//    [self.filterArr addObject:imageSoftEleganceFilter];
    
    // 66 图像低于某值时显示为黑
    GPUImageHighPassFilter *imageHighPassFilter = [[GPUImageHighPassFilter alloc] init];
    [self.filterArr addObject:imageHighPassFilter];
    
    // 6 盒状模糊
    GPUImageBoxBlurFilter *imageBoxBlurFilter = [[GPUImageBoxBlurFilter alloc] init];
    [self.filterArr addObject:imageBoxBlurFilter];
    
}
#pragma mark 创建视图
- (void)createView{
    
    UINib *nib = [UINib nibWithNibName:NSStringFromClass([PhotoVideoFilterCollectionViewCell class]) bundle:nil];
    [self.filterCollectionView registerNib:nib forCellWithReuseIdentifier:NSStringFromClass([PhotoVideoFilterCollectionViewCell class])];
    
    
    self.showImageView.image = self.image;
    self.filterImage = self.image;
}
#pragma mark CollectionView的协议方法
// 个数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.filterNameArr.count;
}
// 创建Cell
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    PhotoVideoFilterCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([PhotoVideoFilterCollectionViewCell class]) forIndexPath:indexPath];
    cell.showFilterImageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"fli3%0.2ld",indexPath.row]];
    cell.showFilterName.text = self.filterNameArr[indexPath.row];
    
    return cell;
}
// 点击方法
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row != 0){
        NSLog(@"%ld",indexPath.row);
        GPUImageOutput<GPUImageInput> *filter = self.filterArr[indexPath.row - 1];
        self.filterImage = [filter imageByFilteringImage:self.image];
    }
    else{
        self.filterImage = self.image;
    }
    self.showImageView.image = self.filterImage;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UI方法
#pragma mark 返回上一页的按钮
- (IBAction)backButton:(id)sender {
    if(self.isEdit){
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"是否保存当前图片" message:@"" preferredStyle:UIAlertControllerStyleAlert];
        // Action
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            UIImageWriteToSavedPhotosAlbum(self.filterImage, nil, nil, nil);
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }];
        // CancelAction
        UIAlertAction *cancelAction = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            
            [self dismissViewControllerAnimated:YES completion:^{
            }];
        }];
        
        // 添加
        [alert addAction:cancelAction];
        [alert addAction:action];
        
        // 显示
        [self presentViewController:alert animated:YES completion:^{
        }];
    }
}
#pragma mark 保存按钮
- (IBAction)saveButton:(id)sender {
    UIImageWriteToSavedPhotosAlbum(self.filterImage, nil, nil, nil);
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"已保存" message:@"" preferredStyle:UIAlertControllerStyleAlert];
    // Action
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self dismissViewControllerAnimated:YES completion:^{
        }];
    }];
    // 添加
    [alert addAction:action];
    // 显示
    [self presentViewController:alert animated:YES completion:^{
    }];
}

@end
