//
//  PhotoVideoViewController.h
//  06 美拍相机
//
//  Created by dllo on 16/3/18.
//  Copyright © 2016年 dllo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PhotoVideoViewController : UIViewController
// 保存图片的数组
@property(nonatomic, strong)NSMutableArray *imageArr;

// 保存视频的地址
@property(nonatomic, strong)NSURL *videoURL;

@end
